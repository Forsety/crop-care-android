package mobi.efarmer.crop_care;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.View;

import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import mobi.efarmer.crop_care.fragment.ForecastFragment;
import mobi.efarmer.crop_care.fragment.HowAreYouDialog;
import mobi.efarmer.crop_care.fragment.LoginFragment;

/**
 * Created by Fors on 13.04.2015.
 */
public class MainActivity extends FragmentActivity implements OnMapReadyCallback, ViewPager.OnPageChangeListener {

    private GoogleMap gMap; // Might be null if Google Play services APK is not available.
    private MainPagerAdapter pagerAdapter;
    private ViewPager pager;
    private PagerSlidingTabStrip pagerTabs;

    private ForecastFragment forecastFragment;

    private boolean running;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        pagerTabs = (PagerSlidingTabStrip) findViewById(R.id.pager_tabs);
        pagerTabs.setViewPager(pager);
        pagerTabs.setOnPageChangeListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            getActionBar().setElevation(0);
        forecastFragment = (ForecastFragment) getSupportFragmentManager().findFragmentById(R.id.frg_forecast);
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, HelpMeActivity.class));
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (running)
                    new HowAreYouDialog().show(getFragmentManager(), null);
            }
        }, (long) (5000 + Math.random() * 10000));
    }

    @Override
    protected void onResume() {
        super.onResume();
        running = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        running = false;
    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        this.gMap = gMap;
        gMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public void onPageSelected(int position) {
        setTitle(pagerAdapter.getPageTitle(position));
        if (position == 0) {
            forecastFragment.expand();
            if (gMap == null)
                ((SupportMapFragment) pagerAdapter.getItem(position)).getMapAsync(this);
        } else
            forecastFragment.collapse(true);
    }

    @Override public void onPageScrollStateChanged(int state) {}
    @Override public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    private class MainPagerAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.IconTabProvider {

        public MainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position) {
                case 0:
                    return SupportMapFragment.newInstance(new GoogleMapOptions()
                            .liteMode(true)
                            .mapType(GoogleMap.MAP_TYPE_HYBRID)
                            .camera(CameraPosition.fromLatLngZoom(new LatLng(51.695620, 8.612653), 14)));
                case 1:
                    return new ForecastFragment();
                case 2:
                    return new LoginFragment();
            }
            return new Fragment();
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.title_main_forecast);
                case 1:
                    return getString(R.string.title_main_database);
                case 2:
                    return getString(R.string.title_main_account);
            }
            return "";
        }

        @Override
        public int getPageIconResId(int i) {
            switch (i) {
                case 0:
                    return R.drawable.ic_home_selector;
                case 1:
                    return R.drawable.ic_database_selector;
                case 2:
                    return R.drawable.ic_person_selector;
            }
            return R.drawable.common_signin_btn_icon_light;
        }
    }

}
