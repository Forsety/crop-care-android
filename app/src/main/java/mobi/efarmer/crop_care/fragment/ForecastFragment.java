package mobi.efarmer.crop_care.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Shader;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import mobi.efarmer.crop_care.R;
import mobi.efarmer.crop_care.model.Demo;
import mobi.efarmer.crop_care.model.Disease;

/**
 * Created by Fors on 13.04.2015.
 */
public class ForecastFragment extends Fragment implements View.OnTouchListener, GestureDetector.OnGestureListener {

    private RecyclerView list;
    private GestureDetector gestureDetector;
    private boolean collapsed = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forecast, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        list = (RecyclerView) view.findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.setAdapter(new ForecastAdapter(Arrays.asList(Demo.DISEASES)));
        list.setOnTouchListener(this);
        gestureDetector = new GestureDetector(getActivity(), this);
    }

    public RecyclerView getList() {
        return list;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (collapsed) {
            expand();
            return true;
        }
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (!collapsed && ((LinearLayoutManager) list.getLayoutManager()).findFirstCompletelyVisibleItemPosition() == 0 && (e2.getY() - e1.getY()) > getView().getHeight() / 3) {
            collapse(false);
            return true;
        }
        return false;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView icon;
        private final TextView title;
        private final RatingBar rating;
        private final TextView tag;

        private ViewHolder(View v) {
            super(v);
            this.icon = (ImageView) v.findViewById(R.id.icon);
            this.title = (TextView) v.findViewById(android.R.id.text1);
            this.rating = (RatingBar) v.findViewById(R.id.rating);
            this.tag = (TextView) v.findViewById(R.id.tag);
        }
    }

    public void expand() {
        final View v = getView();
        if (v != null) {
            final int targetHeight = getResources().getDimensionPixelSize(R.dimen.disease_item_height) * 4;
            v.getLayoutParams().height = 0;
            Animation a = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    v.getLayoutParams().height = (int) (targetHeight * interpolatedTime);
                    v.requestLayout();
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };

            // 1dp/ms
            a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
            v.startAnimation(a);
            v.setVisibility(View.VISIBLE);
            collapsed = false;
        }
    }

    public void collapse(final boolean hide) {
        final View v = getView();
        if (v != null) {
            final int desiredHeight = hide ? 0 : getResources().getDimensionPixelSize(R.dimen.disease_item_height);
            final int initialHeight = v.getMeasuredHeight();

            Animation a = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    if (interpolatedTime == 1 && hide)
                        v.setVisibility(View.GONE);
                    v.getLayoutParams().height = initialHeight - (int) ((initialHeight - desiredHeight) * interpolatedTime);
                    v.requestLayout();
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };

            // 1dp/ms
            a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
            v.startAnimation(a);
            collapsed = true;
        }
    }

    private class ForecastAdapter extends RecyclerView.Adapter<ViewHolder> {

        private List<Disease> diseaseList;

        public ForecastAdapter(List<Disease> diseaseList) {
            this.diseaseList = diseaseList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.disease_item, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Disease disease = diseaseList.get(position);
            holder.icon.setImageURI(disease.getImage());
            holder.title.setText(disease.getName());
            int risk = disease.getRisk();
            if (risk < 2)
                holder.rating.setProgressDrawable(buildRatingBarDrawables(
                        BitmapFactory.decodeResource(getResources(), R.drawable.risk_green_empty),
                        BitmapFactory.decodeResource(getResources(), R.drawable.risk_green_empty),
                        BitmapFactory.decodeResource(getResources(), R.drawable.risk_green)
                ));
            else if (risk < 4)
                holder.rating.setProgressDrawable(buildRatingBarDrawables(
                        BitmapFactory.decodeResource(getResources(), R.drawable.risk_yellow_empty),
                        BitmapFactory.decodeResource(getResources(), R.drawable.risk_yellow_empty),
                        BitmapFactory.decodeResource(getResources(), R.drawable.risk_yellow)
                ));
            else
                holder.rating.setProgressDrawable(buildRatingBarDrawables(
                        BitmapFactory.decodeResource(getResources(), R.drawable.risk_red_empty),
                        BitmapFactory.decodeResource(getResources(), R.drawable.risk_red_empty),
                        BitmapFactory.decodeResource(getResources(), R.drawable.risk_red)
                ));
            holder.rating.setRating(disease.getRisk());
            holder.tag.setText(disease.getType());
        }

        @Override
        public int getItemCount() {
            return diseaseList.size();
        }

        private Drawable buildRatingBarDrawables(Bitmap... images) {
            final int[] requiredIds = {android.R.id.background,
                    android.R.id.secondaryProgress, android.R.id.progress};
            final float[] roundedCorners = new float[]{5, 5, 5, 5, 5, 5, 5, 5};
            Drawable[] pieces = new Drawable[3];
            for (int i = 0; i < 3; i++) {
                ShapeDrawable sd = new ShapeDrawable(new RoundRectShape(
                        roundedCorners, null, null));
                BitmapShader bitmapShader = new BitmapShader(images[i],
                        Shader.TileMode.REPEAT, Shader.TileMode.CLAMP);
                sd.getPaint().setShader(bitmapShader);
                ClipDrawable cd = new ClipDrawable(sd, Gravity.LEFT,
                        ClipDrawable.HORIZONTAL);
                if (i == 0) {
                    pieces[i] = sd;
                } else {
                    pieces[i] = cd;
                }
            }
            LayerDrawable ld = new LayerDrawable(pieces);
            for (int i = 0; i < 3; i++) {
                ld.setId(i, requiredIds[i]);
            }
            return ld;
        }

    }

}
