package mobi.efarmer.crop_care.model;

import android.net.Uri;

import mobi.efarmer.crop_care.BuildConfig;

/**
 * Created by Fors on 13.04.2015.
 */
public class Demo {

    public static final Disease[] DISEASES = new Disease[]{
            new Disease("Yellow rust", "Fungal", encodeResource("drawable/photo_yellorust"), 4),
            new Disease("Dwarf bunt", "Fungal", encodeResource("drawable/photo_dwarfbunt"), 3),
            new Disease("Brome mosaic", "Viral", encodeResource("drawable/photo_bromemosaic"), 2),
            new Disease("Aphid", "Insect", encodeResource("drawable/photo_aphid"), 1),
    };

    public static Uri encodeResource(String resource) {
        return Uri.parse("android.resource://" + BuildConfig.APPLICATION_ID + "/" + resource);
    }

}
