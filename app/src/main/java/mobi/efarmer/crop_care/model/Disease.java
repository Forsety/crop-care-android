package mobi.efarmer.crop_care.model;

import android.net.Uri;

/**
 * Created by Fors on 13.04.2015.
 */
public class Disease {

    private String name;
    private String type;
    private Uri image;
    private int risk;

    public Disease(String name, String type, Uri image, int risk) {
        this.name = name;
        this.type = type;
        this.image = image;
        this.risk = risk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Uri getImage() {
        return image;
    }

    public void setImage(Uri image) {
        this.image = image;
    }

    public int getRisk() {
        return risk;
    }

    public void setRisk(int risk) {
        this.risk = risk;
    }
}
